$(document).ready(function(){
    $('#accordion').accordion()({
        active: false,
        collapsible :true,
    })

    document.getElementById('buttonChange').onclick = changeTheme;

    function changeTheme() {
        document.getElementsByTagName('body')[0].style.backgroundColor = 'black'; 
        document.getElementsByTagName('body')[0].style.color = 'white'; 
    }
})