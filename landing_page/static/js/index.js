$(document).ready(function(){
    $('#accordion').accordion({
        active: false,
        collapsible :true,
        animate : 300
    })

    var color = ["black", "white"];
    var i = -1;
    $("input").click(function(){
        i = i < color.length-1 ? ++i : 0;

        $("body, .ui-widget-content").css({"background" : color[i]});
        $("#accordion h3").css({"background" : color[i], "color" : color[(i+1)%2]});
        $("#accordion .ui-accordion-header-icon").css({"border-color" : color[(i+1)%2]});
        $("#accordion .ui-state-default").css({"border-color" : color[(i+1)%2]});
        $("h1, h2, h3, h4, h5, p, .ui-state-default").css({"color" : color[(i+1)%2]});
        $(".slider.round").css({"border-color" : color[(i+1)%2]});

        if(color[i] == "white"){
            $("#theme-text").html('<strong>Dark Mode</strong>');
        }
        else{
            $("#theme-text").html('<strong>Light Mode</strong>');
        }
        $(".slider .round").inner({"background-color" : color[(i+1)%2]});
    })

})